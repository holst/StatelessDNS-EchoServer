#include "statelessResponse.h"
#include <stdio.h>
#include <string.h>
#include <ldns/ldns.h>

#include "queryParser.h"
#include "methodTemplates.h"
#include "zonesign.h"

int statelessResponse( ldns_pkt *_dnsPkt, int _verboseLevel ) {
  size_t n;
  int rrAmount;
  ldns_rr_list *questions;
  ldns_rr *rr;
  ldns_rdf *fqn;
  char *fqnStr;
  query* q;

  /* Set response bit */
  ldns_pkt_set_qr( _dnsPkt, true );

  /* Iterate through all questions (TODO multiple possible?) */
  questions = ldns_pkt_question( _dnsPkt );
  assert( questions != NULL );
  for( n = 0; n < ldns_rr_list_rr_count( questions ); n++ ) {
    rr = ldns_rr_list_rr( questions, n );
    assert( rr != NULL );

    /* extract fqn */
    fqn = ldns_rr_owner( rr );
    assert( fqn != NULL );
    fqnStr = ldns_rdf2str( fqn );
    printf( "==> %s(%d) :: ", fqnStr, ldns_rr_get_type(rr) );
    fflush( stdout );

    /* Search in zone */
    rrAmount = zoneSignResponse( rr, _dnsPkt );
    if( rrAmount >= 0 ) {
      if( rrAmount > 0 && ldns_pkt_edns_do( _dnsPkt ) ) {
        zoneSignPacket( _dnsPkt );
      }
      printf( "Answers in zonefile\n" );
      continue;
    }
    else {
      /* Parse question */
      q = parseQuery( fqnStr );
      free( fqnStr );
      if( !q ) {
        ldns_pkt_set_rcode( _dnsPkt, LDNS_RCODE_NXDOMAIN );
        // NSEC
        if( ldns_pkt_edns_do( _dnsPkt ) ) {
          zoneGenerateNegativeResponse( _dnsPkt );
        }
      }
      else {

        /* Generate answer */
        rrAmount = generateResponse( q, _dnsPkt );
        if( rrAmount ) {
          printf( "Accepted(%d rr responded)", rrAmount );
        }
        else {
          puts( " -> Failure! (Generate Answer)" );
          ldns_pkt_set_rcode( _dnsPkt, LDNS_RCODE_NXDOMAIN );
        }
      }
    }

    /* Sign packet */
    if( ldns_pkt_edns_do( _dnsPkt ) ) {
      if( zoneSignPacket( _dnsPkt ) >= 0 ) {
        printf( " +dnssec" );
      }
    }
    puts("");
    fflush(stdout);


    //TODO Add debug option as argument
    if( _verboseLevel ) {
      ldns_pkt_print( stdout, _dnsPkt );
    }
  }

  return 0;
}
