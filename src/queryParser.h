#ifndef QUERYPARSER_H
#define QUERYPARSER_H


typedef struct query {
  char *raw;
  short dataIsIp;
  char *data;
  char *method;
  char *alias;
  char *base;
} query;

query* queryNew();
query* queryNewDummy(); // Dummy query with pseudo valid attributes set
void queryPrint( const query *_q );
void queryFree( query *_q );
char* queryGetAttribute( const query *_q, const char *_attr );

query* parseQuery( const char *_query );


/* Initialize/cleanup parser (Need initialized methodTemplates) */
void queryParserInit( const char *_base, const char *_hostip );
void queryParserCleanup();

#endif //QUERYPARSER_H
