#define _GNU_SOURCE // to enable strcasestr

#include "methodTemplates.h"
#include "queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <dirent.h>
#include <ldns/ldns.h>

#define MAX_TEMPLATE_NAME 20

/* Template data type declaration
 * Read templates from files and store them
 * in 'method-list' (filenames) and each gets
 * a 'template-list' */
typedef struct template {
  STAILQ_ENTRY( template ) next;
  char *tmplt;
} template;
STAILQ_HEAD( templates, template );
typedef struct templates templates;

typedef struct method {
  SLIST_ENTRY( method ) next;
  char *name;
  templates response[3];
} method;
SLIST_HEAD( methods, method );
typedef struct methods methods;
#define METHOD_ANS( m ) &m->response[0]
#define METHOD_AUTH( m ) &m->response[1]
#define METHOD_ADD( m ) &m->response[2]
#define METHOD_MAP_LDNS_SECTION( s ) \
  2 == s ?    LDNS_SECTION_ADDITIONAL : \
  1 == s ?    LDNS_SECTION_AUTHORITY : \
  /*default*/ LDNS_SECTION_ANSWER
#define METHOD_RESPONSES { "answer", "authority", "additional" }
#define METHOD_RESPONSE_AMOUNT 3
// TODO prevent/reduce defines?




/* Prototypes */
static char* templateToStr( const char *_template, const query *_q );
static char* getTemplateKey( const char**_template );
static char* getValue( const char *_key );
static method* getMethod( const char *_fileName );
static bool isValidTemplate( const char *_template );
static bool isEmptyLine( const char *_line );

/* Module globals */
static methods g_methods;
static query *g_dummyQuery;


/* Initialize the method list */
void methodTemplatesInit( const char* _methodDir ) {
  char filePath[_POSIX_PATH_MAX];
  size_t dirPathLen;
  DIR *dir;
  struct dirent *entry;
  method *newMethod;

  /* Initialize method structure */
  SLIST_INIT( &g_methods );

  /* Prepare file paths */
  dirPathLen = strlen( _methodDir );
  if( dirPathLen + 3 >= _POSIX_PATH_MAX ) { // path + '/' + file + '\0'
    fprintf( stderr, "Invalid method path (too long)" );
    return;
  }
  (void)strncpy( filePath, _methodDir, _POSIX_PATH_MAX );

  /* set dummy query */
  g_dummyQuery = queryNewDummy();

  /* travers through all file in method directory */
  if( NULL != (dir = opendir( filePath )) ) {
    filePath[dirPathLen++] = '/';
    while( NULL != (entry = readdir( dir )) ) {
      if( DT_REG & entry->d_type ) {
        /* generate current file path */
        (void)strncpy( filePath+dirPathLen, entry->d_name, _POSIX_PATH_MAX-dirPathLen );
        filePath[_POSIX_PATH_MAX-1] = '\0';

        /* create new method */
        newMethod = getMethod( filePath );
        if( !newMethod ) {
          fprintf( stderr, "Empty or invalid method file '%s'", filePath );
          continue;
        }
        newMethod->name = strdup( entry->d_name );
        SLIST_INSERT_HEAD( &g_methods, newMethod, next );
      }
    }
    closedir( dir );
  }
  else {
    perror( filePath );
    queryFree( g_dummyQuery );
    return;
  }
}

void methodTemplatesCleanup() {
  int i;
  method *m;
  template *t;

  while( !SLIST_EMPTY( &g_methods ) ) {
    m = SLIST_FIRST( &g_methods );
    for( i = 0; i < METHOD_RESPONSE_AMOUNT; ++i ) {
      while( !STAILQ_EMPTY( &m->response[i] ) ) {
        t = STAILQ_FIRST( &m->response[i] );
        STAILQ_REMOVE_HEAD( &m->response[i], next );
        free( t->tmplt );
        free( t );
      }
    }
    SLIST_REMOVE_HEAD( &g_methods, next );
    free( m->name );
    free( m );
  }
}

/* Generate string over all methods seperated through delimiter (allocate memory)*/
char* getMethodsStr( char _delimiter ) {
  char buffer[1024];
  method *m;
  size_t len, cur = 0;

  SLIST_FOREACH( m, &g_methods, next ) {
    len = strlen( m->name );
    if( len + cur  + 1 >= 1024 ) {
      fprintf( stderr, "Increase method buffer to get all methods" );
      break;
    }
    strncpy( &buffer[cur], m->name, len );
    cur += len;
    buffer[cur++] = _delimiter;
  }

  if( 0 == cur ) {
    return NULL;
  }
  buffer[--cur] = '\0';
  return strndup( buffer, cur );
}

bool isValidTemplate( const char *_template ) {
  assert( g_dummyQuery != NULL );
  assert( _template != NULL );

  ldns_status status;
  ldns_rr *rr;
  char *rrStr = templateToStr( _template, g_dummyQuery );
  if( NULL != rrStr ) {
    status = ldns_rr_new_frm_str( &rr, rrStr, 0, NULL, NULL );
    if( status == LDNS_STATUS_OK ) {
      ldns_rr_free( rr );
      free( rrStr );
      return true;
    }
  }
  return false;
}

bool isEmptyLine( const char *_line ) {
  assert( _line != NULL );

  const char *p = _line;

  while( '\0' != *p++ ) {
    if( ' ' != *p && '\t' != *p ) {
      return false;
    }
  }

  return true;
}

method* getMethod( const char *_fileName ) {
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  size_t lineCount = 0, templateCount = 0;
  size_t r;
  int i;
  method *retMethod;
  template *newTemplate;
  templates *curResponse;
  static char *responses[] = METHOD_RESPONSES;

  /* open file readonly */
  if( NULL == (fp = fopen( _fileName, "r" )) ) {
    perror( _fileName );
    return NULL;
  }

  /* Initialize method */
  retMethod = malloc( sizeof( method ) );
  for( i = 0; i < METHOD_RESPONSE_AMOUNT; ++i ) {
    STAILQ_INIT( &retMethod->response[i] );
  }
  curResponse = METHOD_ANS( retMethod );

  /* Insert templates line by line */
  while( -1 != (r= getline( &line, &len, fp )) ) {
    lineCount++;
    /* remove newline character */
    if( r && '\n' == line[r-1] ) {
      line[r-1] = '\0';
    }

    if( isEmptyLine( line ) ) {
      continue;
    }

    /* select response section */
    if( ';' == line[0] ) {
      for( i = 0; i < METHOD_RESPONSE_AMOUNT; ++i ) {
        if( strcasestr( line, responses[i] ) ) {
          curResponse = &retMethod->response[i];
          break;
        }
      }
      continue; // All other lines started with # are threated as comments
    }

    /* test template */
    if( !isValidTemplate( line ) ) {
      /* quick and dirty line output beautifier */
      for( i = 0; i < r && i < 20; ++i ) line[i] = line[i] < 33 ? '.' : line[i];
      line[--i] = '\0';

      fprintf( stderr, "Invalid line in file '%s:%lu': %s\n", _fileName, lineCount, line );
      continue;
    }

    newTemplate = malloc( sizeof( template ) );
    newTemplate->tmplt = strdup( line );
    STAILQ_INSERT_TAIL( curResponse, newTemplate, next );
    templateCount++;
  }
  free( line );
  fclose( fp );

  /* Only return method, if at least one valid template set */
  if( !templateCount ) {
    free( retMethod );
    return NULL;
  }
  return retMethod;
}


char* templateToStr( const char *_template, const query *_q ) {
  char buffer[1024];
  char *key, *value;
  char *p = buffer;
  const char *t = _template;
  size_t valueLen;
  int l = 0;

  while( l < 1024 && *t != '\0' ) {
    if( *t == '<' ) {
      /* template replacement */
      // TODO error handling (especially buffer overflow)
      key = getTemplateKey( &t );
      if( !key ) {
        return NULL;
      }
      value = queryGetAttribute( _q, key );
      if( NULL == value ) {
        fprintf( stderr, "No attribute '%s'\n", key );
        free( key );
        return NULL;
      }
      valueLen = strlen( value );
      strncpy( p, value, valueLen );
      p += valueLen;
      l += valueLen;

      free( key );
    }
    else {
      *p++ = *t++;
      l++;
    }
  }
  
  buffer[l] = '\0';
  return strndup( buffer, l );
}

/* Return key of "<key>"-string (allocate memory)
 * and set template pointer after "<key>"-string */
char* getTemplateKey( const char **_template ) {
  assert( NULL != _template );
  assert( NULL != *_template );
  assert( '<' == **_template );

  char *key;
  const char *t = *_template;
  int i;
  for( i = 0; *t != '>'; ++i, ++t ) {
    if( i >= MAX_TEMPLATE_NAME ) {
      fprintf( stderr, "TemplateKey to long (max %d)\n", MAX_TEMPLATE_NAME );
      return NULL;
    }
    if( '\0' == *t ) {
      fprintf( stderr, "No matching TemplateKey brace\n" );
      return NULL;
    }
  }

  /* extract key */
  key = strndup( *_template + 1, i-1 );

  /* store new address */
  *_template = ++t;

  return key;
}


int generateResponse( const query *_q, ldns_pkt *_pkt ) {
  method *m;
  template *t;
  int i = 0, rrCount = 0;
  char *rrStr;
  ldns_rr_list *rrList[METHOD_RESPONSE_AMOUNT] = { NULL };
  ldns_rr *rr;
  ldns_status status;

  /* search method */
  SLIST_FOREACH( m, &g_methods, next ) {
    if( !strcmp( m->name, _q->method ) ) {
      /* generate per section */
      for( i = 0; i < METHOD_RESPONSE_AMOUNT; ++i ) {
        if( STAILQ_EMPTY( &m->response[i] ) ) {
          continue;
        }

        rrList[i] = ldns_rr_list_new();
        if( !rrList[i] ) {
          fprintf( stderr, "Can't allocate rr-list\n" );
          break;
        }

        STAILQ_FOREACH( t, &m->response[i], next ) {
          rrStr = templateToStr( t->tmplt, _q );
          if( !rrStr ) {
            break;
          }
          status = ldns_rr_new_frm_str( &rr, rrStr, 0, NULL, NULL );
          if( status != LDNS_STATUS_OK ) {
            fprintf( stderr, "rr_new_frm_str(): %s\n", ldns_get_errorstr_by_id( status ) );
            break;
          }
          if( !ldns_rr_list_push_rr( rrList[i], rr ) ) {
            fprintf( stderr, "rr_list_push_rr(): %s\n", ldns_get_errorstr_by_id( status ) );
            break;
          }
          rrCount++;
          free( rrStr );
        }
      }
      break;
    }
  }

  /* Sections not complete */
  if( METHOD_RESPONSE_AMOUNT != i ) {
    for( i = 0; i < METHOD_RESPONSE_AMOUNT; ++i ) {
      free( rrList[i] );
    }
    return 0;
  }

  /* Push all section to package */
  if( rrCount ) {
    for( i = 0; i < METHOD_RESPONSE_AMOUNT; ++i ) {
      if( !ldns_pkt_safe_push_rr_list( _pkt, METHOD_MAP_LDNS_SECTION( i ), rrList[i] ) ) {
        // TODO remove previous added rrs
        fprintf( stderr, "Can't add rr-list to section\n" );
        return 0;
      }
    }
  }

  return rrCount;
}
