#define _GNU_SOURCE // to enable strcasestr

#include "queryParser.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <regex.h>
#include <ldns/ldns.h>

#include "methodTemplates.h"


static bool validIp( const char *_ipStr );


static char *g_base = NULL;
static int g_baseLen;
static char *g_hostip = NULL;
static regex_t g_regex;

query* queryNew() {
  return calloc( 1, sizeof( query ) );
}

query* queryNewDummy() {
  query *q = malloc( sizeof( query ) );

  q->raw = strdup( "dummy" );
  q->base = strdup( "dummy" );
  q->method = strdup( "dummy" );
  q->alias = strdup( "dummy" );
  q->data = strdup( "1.2.3.4" );
  q->dataIsIp = true;

  return q;
}

void queryPrint( const query *_q ) {
  printf( "Data:   %s %s\n", _q->data ? _q->data : "(null)", _q->dataIsIp ? "(ip)" : "" );
  printf( "Method: %s\n", _q->method ? _q->method : "(null)" );
  printf( "Alias:  %s\n", _q->alias ? _q->alias : "(null)" );
  printf( "Base:   %s\n", _q->base ? _q->base : "(null)" );
}

void queryFree( query *_q ) {
  if( !_q ) return;

  if( _q->data ) free( _q->data );
  if( _q->method ) free( _q->method );
  if( _q->alias ) free( _q->alias );
  if( _q->base ) free( _q->base );
  if( _q->raw ) free( _q->raw );

  free( _q );
}

char* queryGetAttribute( const query *_q, const char *_attr ) {
  assert( _q != NULL );
  assert( _attr != NULL );
  // TODO better implementation
  if(      !strcmp( _attr, "base" ) ) return _q->base;
  else if( !strcmp( _attr, "alias" ) ) return _q->alias;
  else if( !strcmp( _attr, "data" ) ) return _q->data;
  else if( !strcmp( _attr, "ip" ) ) return _q->dataIsIp ? _q->data : NULL;
  else if( !strcmp( _attr, "hostip" ) ) return g_hostip ? g_hostip : strdup("1.2.3.4");
  else if( !strcmp( _attr, "query" ) ) return _q->raw;
  else {
    fprintf( stderr, "No attribute named '%s'\n", _attr );
    return NULL;
  }
}

bool validIp( const char *_ipStr ) {
  ldns_rdf *ip = ldns_rdf_new_frm_str( LDNS_RDF_TYPE_A, _ipStr );
  if( ip ) {
    ldns_rdf_free( ip );
    return true;
  }
  return false;
}

query* parseQuery( const char *_query ) {
  regmatch_t hits[4]; // first match is complete query
  const char *p, *base, *cur;
  size_t len;
  int status;
  query *q;

  // (hit[1] -> data).(hit[2] -> method).(hit[3] -> alias/base)
  if( 0 != (status = regexec( &g_regex, _query, 4, hits, 0 )) ) {
    fprintf( stderr, "Invalid query: No match\n" );
    return NULL;
  }
  q = queryNew();
  q->method = strndup( &_query[hits[2].rm_so], hits[2].rm_eo - hits[2].rm_so );

  // process base
  cur = &_query[hits[3].rm_so];
  if( NULL == (base = strcasestr( cur, g_base )) ) {
    fprintf( stderr, "Invalid query: Wrong base\n" );
    queryFree( q );
    return NULL;
  }
  if( '\0' != base[g_baseLen] &&
      !('.' == base[g_baseLen] && '\0' == base[g_baseLen+1]) ) {
    fprintf( stderr, "Invalid query: '%s' after base\n", &base[g_baseLen] );
    queryFree( q );
    return NULL;
  }
  q->base = strdup( g_base );

  // process alias
  /* q->alias = NULL; */
  /* len = base - cur; */
  /* if( len > 1 ) { */
  /*   p = strchr( cur, '.' ); */
  /*   q->alias = strndup( cur, p - cur ); */
  /* } */
  q->alias = strndup( cur, base - cur - 1 );

  // process data
  q->data = strndup( _query, hits[1].rm_eo - hits[1].rm_so );
  q->dataIsIp = validIp( q->data );

  // save raw query
  q->raw = strdup( _query );

  return q;
}

void queryParserInit( const char *_base, const char *_hostip ) {
  char buffer[1024];
  char *methodStr;
  size_t len, cur = 0;
  int status;

  /* save base */
  g_baseLen = strlen( _base );
  if( '.' == _base[g_baseLen-1] ) {
    g_baseLen--;
  }
  g_base = strndup( _base, g_baseLen );
  /* save ip */
  g_hostip = strdup(_hostip);

  /* construct regex str (TODO handle buffer overflow) */
  (void)strcpy( buffer, "^(.*)\\.(" );
  cur += 8;

  methodStr = getMethodsStr( '|' );
  (void)strcat( &buffer[cur], methodStr );
  cur += strlen( methodStr );

  (void)strcat( &buffer[cur], ")\\.(.*)$" );
  cur += 9;

  /* compile regex */
  if( 0 != (status = regcomp( &g_regex, buffer, REG_EXTENDED )) ) {
    //TODO Get error with regerror
    fprintf( stderr, "Can't compile regex" );
    return;
  }
}

void queryParserCleanup() {
  free( g_base );
  free( g_hostip );
  regfree( &g_regex );
}
